package com.silencestudios.gdxengine.instance;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.silencestudios.gdxengine.component.ID;
import com.silencestudios.gdxengine.component.annotations.Dependency;

import java.lang.reflect.Field;

public class EntityInjector {

    private Engine engine;

    public EntityInjector(Engine engine) {
        this.engine = engine;
    }

    public void process(Entity entity) {
        Class<?> clazz = entity.getClass();

        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(Dependency.class)) {
                String id = field.getAnnotation(Dependency.class).ID();

                for (Entity e : engine.getEntities()) {
                    Class<?> entityClass = e.getClass();

                    if (entityClass.isAnnotationPresent(Dependency.class)) {
                        String entityId = entityClass.getAnnotation(Dependency.class).ID();
                        if (entityId.equals(id)) {
                            field.setAccessible(true);

                            try {
                                field.set(entity, e);
                            } catch (IllegalAccessException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }

                    ID entityId = e.getComponent(ID.class);
                    if (entityId != null) {
                        if (id.equals(entityId.value)) {
                            field.setAccessible(true);
                            try {
                                field.set(entity, e);
                            } catch (IllegalAccessException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }
}
