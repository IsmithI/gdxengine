package com.silencestudios.gdxengine.component.annotations;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.silencestudios.gdxengine.component.Provider;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
public @interface ComponentProvider {
    Class<? extends Provider> of() default Default.class;

    class Default extends Provider {

        protected Default(Entity entity) {
            super(entity);
        }

        @Override
        public Component get() {
            return null;
        }
    }
}
