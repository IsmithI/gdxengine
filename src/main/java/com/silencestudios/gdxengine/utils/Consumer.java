package com.silencestudios.gdxengine.utils;

public interface Consumer<T> {
    void accept(T t);
}
