package com.silencestudios.gdxengine.component;

import com.badlogic.ashley.core.Entity;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;

public class TestEntity extends Entity {

    @ComponentProvider
    private Transform transform;

}
