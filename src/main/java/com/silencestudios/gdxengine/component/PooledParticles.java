package com.silencestudios.gdxengine.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;

import java.util.ArrayList;
import java.util.List;

public class PooledParticles implements Component {

    ParticleEffectPool particleEffectPool;
    List<ParticleEffectPool.PooledEffect> effects = new ArrayList<>();

    public PooledParticles(ParticleEffect particleEffect, int initialCapacity, int max) {
        particleEffectPool = new ParticleEffectPool(particleEffect, initialCapacity, max);
    }

    public ParticleEffectPool.PooledEffect create() {
        ParticleEffectPool.PooledEffect pooledEffect = particleEffectPool.obtain();
        effects.add(pooledEffect);
        return pooledEffect;
    }

    public void reset() {
        for (ParticleEffectPool.PooledEffect effect : effects) {
            effect.free();
        }
        effects.clear();
    }
}
