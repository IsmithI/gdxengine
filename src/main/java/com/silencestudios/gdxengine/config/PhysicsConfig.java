package com.silencestudios.gdxengine.config;

import com.badlogic.gdx.math.Vector2;

public class PhysicsConfig {

    private static PhysicsConfig instance = null;
    private Vector2 gravity = new Vector2(0, -10f);
    private float stepDt = 1 / 60f;
    private int velocityIterations = 8;
    private int positionIterations = 3;
    private boolean drawDebug = false;

    public static PhysicsConfig get() {
        if (instance == null)
            instance = new PhysicsConfig();

        return instance;
    }

    public Vector2 getGravity() {
        return gravity;
    }

    public void setGravity(Vector2 gravity) {
        this.gravity = gravity;
    }

    public float getStepDt() {
        return stepDt;
    }

    public void setStepDt(float stepDt) {
        this.stepDt = stepDt;
    }

    public int getVelocityIterations() {
        return velocityIterations;
    }

    public void setVelocityIterations(int velocityIterations) {
        this.velocityIterations = velocityIterations;
    }

    public int getPositionIterations() {
        return positionIterations;
    }

    public void setPositionIterations(int positionIterations) {
        this.positionIterations = positionIterations;
    }

    public boolean isDrawDebug() {
        return drawDebug;
    }

    public void setDrawDebug(boolean drawDebug) {
        this.drawDebug = drawDebug;
    }
}
