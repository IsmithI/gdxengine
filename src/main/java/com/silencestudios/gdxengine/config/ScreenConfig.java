package com.silencestudios.gdxengine.config;

import com.badlogic.gdx.graphics.Color;

public class ScreenConfig {
    private static ScreenConfig instance = null;
    public float worldWidth = 16f;
    public float worldHeight = 9f;
    public float guiScale = 1f;
    public boolean centerCamera = true;
    public Color clearScreenColor = new Color(0.2f, 0.2f, 0.2f, 1);

    public static ScreenConfig get() {
        if (instance == null)
            instance = new ScreenConfig();

        return instance;
    }
}
