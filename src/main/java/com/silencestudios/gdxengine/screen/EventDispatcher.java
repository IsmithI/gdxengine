package com.silencestudios.gdxengine.screen;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.silencestudios.gdxengine.component.EventListener;
import com.silencestudios.gdxengine.component.annotations.Debug;
import com.silencestudios.gdxengine.component.annotations.Dependency;
import com.silencestudios.gdxengine.utils.Event;

@Debug
@Dependency(ID = "eventDispatcher")
public class EventDispatcher extends Entity {

    private Engine engine;

    void setEngine(Engine engine) {
        this.engine = engine;
    }

    public <T> void dispatch(Event<T> event) {
        for (int i = 0; i < engine.getSystems().size(); i++) {
            EntitySystem entitySystem = engine.getSystems().get(i);
            if (entitySystem instanceof EventListener) {
                EventListener eventListener = (EventListener) entitySystem;
                if (eventListener.getEventType().equals(event.name)) {
                    eventListener.onEvent(event);
                }
            }
        }
    }

}
