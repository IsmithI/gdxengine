package com.silencestudios.gdxengine.input;

public enum ActionKeys {
    LEFT,
    RIGHT,
    UP,
    DOWN,
    JUMP,
    ACTION1,
    ACTION2,
}
