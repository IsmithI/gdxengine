package com.silencestudios.gdxengine.entity;

import com.badlogic.ashley.core.Entity;
import com.silencestudios.gdxengine.component.AssetsContainer;
import com.silencestudios.gdxengine.component.annotations.Dependency;

@Dependency(ID = "assetManager")
public class Assets extends Entity {
    public AssetsContainer assetsContainer;
}
