package com.silencestudios.gdxengine.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.Batch;

public class BatchContainer implements Component {
    public Batch batch;

    public BatchContainer(Batch batch) {
        this.batch = batch;
    }
}
