package com.silencestudios.gdxengine.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

public abstract class Provider<T extends Component> {

    protected Entity entity;

    public Provider(Entity entity) {
        this.entity = entity;
    }

    public abstract T get();
}
