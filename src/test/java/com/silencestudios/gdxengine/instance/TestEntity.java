package com.silencestudios.gdxengine.instance;

import com.badlogic.ashley.core.Entity;
import com.silencestudios.gdxengine.component.Transform;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;

public class TestEntity extends Entity {

    @ComponentProvider
    private Transform transform;

}
