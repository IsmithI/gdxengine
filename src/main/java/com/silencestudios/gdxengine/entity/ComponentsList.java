package com.silencestudios.gdxengine.entity;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kotcrab.vis.ui.widget.ListView;
import com.silencestudios.gdxengine.component.Lifecycle;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;
import com.silencestudios.gdxengine.component.annotations.Debug;
import com.silencestudios.gdxengine.component.annotations.Dependency;

import java.util.ArrayList;
import java.util.Arrays;

@Debug
@Dependency(ID = "componentsList")
public class ComponentsList extends Entity {

    private Engine engine;

    private ListView<Component> componentListView;
    private ComponentsListAdapter componentsListAdapter;

    private Entity selectedEntity;

    private float timer = 0.2f;

    @ComponentProvider
    public Lifecycle lifecycle() {
        Lifecycle lifecycle = new Lifecycle();

        componentsListAdapter = new ComponentsListAdapter(new ArrayList<>());
        componentListView = new ListView<>(componentsListAdapter);

        lifecycle.onUpdate = dt -> {
            timer -= dt;

            if (timer <= 0) {
                componentsListAdapter.itemsDataChanged();
                timer += 0.2f;
            }
        };

        return lifecycle;
    }

    public Actor getActor() {
        return componentListView.getMainTable();
    }

    public ListView<Component> getComponentListView() {
        return componentListView;
    }

    public void setSelectedEntity(Entity entity) {
        selectedEntity = entity;
        componentsListAdapter.clear();
        ArrayList<Component> components = new ArrayList<>(Arrays.asList(entity.getComponents().toArray()));
        componentsListAdapter.addAll(components);
        componentsListAdapter.itemsChanged();
    }
}
