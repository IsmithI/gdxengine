package com.silencestudios.gdxengine.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.silencestudios.gdxengine.utils.Consumer;

public class CollisionHandler implements Component {

    public Consumer<Entity> onContactEnter = entity -> {
    };
    public Consumer<Entity> onContactLeave = entity -> {
    };
    private Family family;

    public CollisionHandler() {
    }

    public CollisionHandler(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return family;
    }
}
