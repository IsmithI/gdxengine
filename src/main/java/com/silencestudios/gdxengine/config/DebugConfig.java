package com.silencestudios.gdxengine.config;

public class DebugConfig {

    private static DebugConfig instance = null;
    private boolean isEnabled = false;

    public static DebugConfig get() {
        if (instance == null) instance = new DebugConfig();

        return instance;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }
}
