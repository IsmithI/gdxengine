package com.silencestudios.gdxengine.instance;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.assets.AssetManager;
import com.silencestudios.gdxengine.component.ID;

import java.lang.reflect.Field;

public class Instance {

    private static Instance instance = null;
    private Engine engine;
    private EntityInjector entityInjector;
    private ResourceInjector resourceInjector;
    private ComponentInjector componentInjector;

    public Instance(Engine engine, AssetManager assetManager) {
        this.engine = engine;
        entityInjector = new EntityInjector(engine);
        resourceInjector = new ResourceInjector(assetManager);
        componentInjector = new ComponentInjector();
    }

    public static Instance init(Engine engine, AssetManager assetManager) {
        instance = new Instance(engine, assetManager);
        return instance;
    }

    public static Instance get() {
        return instance;
    }

    public Entity create(Class<? extends Entity> clazz) {
        try {
            Entity entity = clazz.newInstance();
            injectEngine(entity);
            resourceInjector.process(entity);
            entityInjector.process(entity);
            componentInjector.process(entity);

            engine.addEntity(entity);

            return entity;

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Entity create(Class<? extends Entity> clazz, String id) {
        try {
            Entity entity = clazz.newInstance();
            entity.add(new ID(id));

            injectEngine(entity);
            resourceInjector.process(entity);
            entityInjector.process(entity);
            componentInjector.process(entity);

            engine.addEntity(entity);

            return entity;

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void injectEngine(Entity entity) {
        Class<?> entityClass = entity.getClass();
        for (Field field : entityClass.getDeclaredFields()) {
            if (field.getType() == Engine.class) {
                field.setAccessible(true);
                try {
                    field.set(entity, engine);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void remove(Entity entity) {
        engine.removeEntity(entity);
    }
}
