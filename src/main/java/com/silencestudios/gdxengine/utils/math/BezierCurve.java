package com.silencestudios.gdxengine.utils.math;

import com.badlogic.gdx.math.Vector2;

import net.dermetfan.utils.math.MathUtils;

import java.util.ArrayList;
import java.util.List;

public class BezierCurve {

    private List<Vector2> points = new ArrayList<>();

    public Vector2 calculate(float dt) {
        Vector2 sum = new Vector2();
        int n = points.size() - 1;
        for (int i = 0; i < n + 1; i++) {
            Vector2 p = points.get(i);
            float b = getBasis(dt, n, i);
            sum.add(Math.round(p.x * b * 100) / 100f, Math.round(p.y * b * 100) / 100f);
        }

        return sum;
    }

    float pow(float a, int n) {
        float s = 1;
        for (int i = 0; i < n; i++) {
            s *= a;
        }
        return s;
    }

    float getBasis(float t, int n, int k) {
        float m = ((float) MathUtils.factorial(n)) / (float) (MathUtils.factorial(k) * MathUtils.factorial(n - k));

        return m * pow(t, k) * pow(1 - t, n - k);
    }

    public List<Vector2> getPoints() {
        return points;
    }

    public void setPoints(List<Vector2> points) {
        this.points = points;
    }
}
