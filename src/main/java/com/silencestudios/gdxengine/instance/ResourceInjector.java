package com.silencestudios.gdxengine.instance;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.assets.AssetManager;
import com.silencestudios.gdxengine.component.annotations.Resource;

import java.lang.reflect.Field;

public class ResourceInjector {

    private AssetManager assetManager;

    public ResourceInjector(AssetManager assetManager) {
        this.assetManager = assetManager;
    }

    public void process(Entity entity) {
        Class<?> clazz = entity.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(Resource.class)) {
                String key = field.getAnnotation(Resource.class).key();
                Class<?> loader = field.getAnnotation(Resource.class).loaderType();

                Object o = loader == Resource.Default.class ?
                        assetManager.get(key) :
                        assetManager.get(key, loader);

                field.setAccessible(true);
                try {
                    field.set(entity, o);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
