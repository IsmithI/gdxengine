package com.silencestudios.gdxengine.entity;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.silencestudios.gdxengine.component.BatchContainer;
import com.silencestudios.gdxengine.component.Lifecycle;
import com.silencestudios.gdxengine.component.TiledMapRendererContainer;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;
import com.silencestudios.gdxengine.component.annotations.Dependency;
import com.silencestudios.gdxengine.component.annotations.Resource;

// Example class, do not use directly!!!
public class Map extends Entity {

    private TiledMap tiledMap;

    @Resource(key = "maps/map.atlas")
    private TextureAtlas textureAtlas;

    @Dependency(ID = "batch")
    private Entity batchEntity;

    @ComponentProvider
    private TiledMapRendererContainer mapRendererContainer;

    @ComponentProvider
    private Lifecycle lifecycle() {
        Lifecycle lifecycle = new Lifecycle();

        lifecycle.onCreate = () -> {
            Batch batch = batchEntity.getComponent(BatchContainer.class).batch;
            mapRendererContainer.init(new OrthogonalTiledMapRenderer(tiledMap, batch));
        };

        return lifecycle;
    }
}
