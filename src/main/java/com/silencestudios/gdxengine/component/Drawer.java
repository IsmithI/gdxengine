package com.silencestudios.gdxengine.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.Batch;

public interface Drawer extends Component {
    void draw(Batch batch, float dt, float x, float y, float scaleX, float scaleY, float rotation);
}
