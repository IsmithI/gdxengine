package com.silencestudios.gdxengine.entity;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.World;
import com.silencestudios.gdxengine.component.annotations.Dependency;

@Dependency(ID = "physicsWorld")
public class PhysicsWorld extends Entity {
    private World world;

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }
}
