package com.silencestudios.gdxengine.component;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;

public class TiledMapRendererContainer implements Drawer {

    private TiledMapRenderer tiledMapRenderer;

    public TiledMapRendererContainer() {
    }

    public TiledMapRendererContainer(TiledMapRenderer tiledMapRenderer) {
        this.tiledMapRenderer = tiledMapRenderer;
    }

    @Override
    public void draw(Batch batch, float dt, float x, float y, float scaleX, float scaleY, float rotation) {
        if (tiledMapRenderer != null)
            tiledMapRenderer.render();
    }

    public void init(TiledMapRenderer tiledMapRenderer) {
        this.tiledMapRenderer = tiledMapRenderer;
    }
}
