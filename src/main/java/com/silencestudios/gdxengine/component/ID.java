package com.silencestudios.gdxengine.component;

import com.badlogic.ashley.core.Component;

public class ID implements Component {

    public final String value;

    public ID(String value) {
        this.value = value;
    }
}
