package com.silencestudios.gdxengine.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.assets.AssetManager;

public class AssetsContainer implements Component {
    private AssetManager assetManager;

    public AssetsContainer(AssetManager assetManager) {
        this.assetManager = assetManager;
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }
}
