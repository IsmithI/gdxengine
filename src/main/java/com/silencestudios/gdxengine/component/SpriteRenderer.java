package com.silencestudios.gdxengine.component;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class SpriteRenderer implements Drawer {

    public static float SPRITE_SCALE = 100f;

    private Sprite sprite;
    public Vector2 origin;
    public Vector2 position = new Vector2();
    public float rotation = 0f;

    public SpriteRenderer(Sprite sprite) {
        setSprite(sprite);
        origin = new Vector2(sprite.getWidth() / 2, sprite.getHeight() / 2);
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
        this.sprite.setSize(sprite.getWidth() / SPRITE_SCALE, sprite.getHeight() / SPRITE_SCALE);
    }

    @Override
    public void draw(Batch batch, float dt, float x, float y, float scaleX, float scaleY, float rotation) {
        sprite.setPosition(x - origin.x + position.x, y - origin.y + position.y);
        sprite.setOrigin(origin.x, origin.y);
        sprite.setScale(scaleX, scaleY);
        sprite.setRotation(rotation + this.rotation);

        sprite.draw(batch);
    }

    public Vector2 getOrigin() {
        return origin;
    }

    public void setOrigin(Vector2 origin) {
        this.origin = origin;
    }
}
