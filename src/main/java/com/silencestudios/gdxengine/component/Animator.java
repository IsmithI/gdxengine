package com.silencestudios.gdxengine.component;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import java.util.Collections;
import java.util.Map;

import static com.silencestudios.gdxengine.component.SpriteRenderer.SPRITE_SCALE;

public class Animator implements Drawer {

    private float stateTime = 0f;
    private Vector2 origin = new Vector2(0, 0);
    private String currentAnimation;
    private Map<String, Animation<TextureRegion>> animations;

    public Animator(Map<String, Animation<TextureRegion>> animations) {
        this.animations = animations;

        for (Animation<TextureRegion> animation : animations.values()) {
            TextureRegion region = animation.getKeyFrames()[0];
            if (region != null) {
                origin.set(region.getRegionWidth() / 2f / SPRITE_SCALE, region.getRegionHeight() / 2f / SPRITE_SCALE);
            }
        }
    }

    public Map<String, Animation<TextureRegion>> getAnimations() {
        return Collections.unmodifiableMap(animations);
    }

    public Animation<TextureRegion> getAnimation() {
        return animations.get(currentAnimation);
    }

    public String getCurrentAnimation() {
        return currentAnimation;
    }

    public void setCurrentAnimation(String currentAnimation) {
        this.currentAnimation = currentAnimation;
    }

    @Override
    public void draw(Batch batch, float dt, float x, float y, float scaleX, float scaleY, float rotation) {
        TextureRegion textureRegion = getAnimation().getKeyFrame(stateTime);
        origin.set(textureRegion.getRegionWidth() / 2f / SPRITE_SCALE, textureRegion.getRegionHeight() / 2f / SPRITE_SCALE);
        origin.x = scaleX < 0 ? -origin.x : origin.x;

        batch.draw(textureRegion, x - origin.x, y - origin.y, 0, 0, textureRegion.getRegionWidth() / SPRITE_SCALE, textureRegion.getRegionHeight() / SPRITE_SCALE, scaleX, scaleY, rotation);

        stateTime += dt;
    }

    public static TextureRegion[] split(Texture texture, int tileWidth, int tileHeight, int columnsCount, int rowsCount) {
        TextureRegion[][] regions = TextureRegion.split(texture, tileWidth, tileHeight);
        TextureRegion[] frames = new TextureRegion[columnsCount * rowsCount];
        int index = 0;
        for (int i = 0; i < rowsCount; i++) {
            for (int j = 0; j < columnsCount; j++) {
                frames[index++] = regions[i][j];
            }
        }

        return frames;
    }

    public static TextureRegion[] split(TextureRegion texture, int tileWidth, int tileHeight, int columnsCount, int rowsCount) {
        TextureRegion[][] regions = texture.split(tileWidth, tileHeight);
        TextureRegion[] frames = new TextureRegion[columnsCount * rowsCount];
        int index = 0;
        for (int i = 0; i < rowsCount; i++) {
            for (int j = 0; j < columnsCount; j++) {
                frames[index++] = regions[i][j];
            }
        }

        return frames;
    }
}
