package com.silencestudios.gdxengine.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.InputMultiplexer;
import com.silencestudios.gdxengine.component.InputProcessorContainer;

public class InputProcessingSystem extends EntitySystem {

    private InputMultiplexer inputMultiplexer;

    public InputProcessingSystem(InputMultiplexer inputMultiplexer) {
        this.inputMultiplexer = inputMultiplexer;
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        engine.addEntityListener(Family.all(InputProcessorContainer.class).get(), new EntityListener() {
            @Override
            public void entityAdded(Entity entity) {
                inputMultiplexer.addProcessor(entity.getComponent(InputProcessorContainer.class).getInputProcessor());
            }

            @Override
            public void entityRemoved(Entity entity) {
                inputMultiplexer.removeProcessor(entity.getComponent(InputProcessorContainer.class).getInputProcessor());
            }
        });
    }
}
