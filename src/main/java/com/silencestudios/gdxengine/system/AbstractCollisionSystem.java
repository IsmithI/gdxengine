package com.silencestudios.gdxengine.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;

public abstract class AbstractCollisionSystem extends EntitySystem {

    private Family familyA, familyB;

    public AbstractCollisionSystem() {
    }

    public AbstractCollisionSystem(Family family) {
        this.familyA = this.familyB = family;
    }

    public AbstractCollisionSystem(Family familyA, Family familyB) {
        this.familyA = familyA;
        this.familyB = familyB;
    }

    boolean canApplyA(Entity entity) {
        if (familyA == null) return true;

        return familyA.matches(entity);
    }

    boolean canApplyB(Entity entity) {
        if (familyB == null) return true;

        return familyB.matches(entity);
    }

    public abstract void handleContactBegin(Entity e1, Entity e2);

    public abstract void handleContactEnd(Entity e1, Entity e2);
}
