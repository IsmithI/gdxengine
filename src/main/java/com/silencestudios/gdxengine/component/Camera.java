package com.silencestudios.gdxengine.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.viewport.Viewport;

public class Camera implements Component {

    private Viewport viewport;
    private com.badlogic.gdx.graphics.Camera camera;

    public com.badlogic.gdx.graphics.Camera getCamera() {
        return camera;
    }

    public void setCamera(com.badlogic.gdx.graphics.Camera camera) {
        this.camera = camera;
    }

    public Viewport getViewport() {
        return viewport;
    }

    public void setViewport(Viewport viewport) {
        this.viewport = viewport;
    }
}
