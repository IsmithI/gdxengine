package com.silencestudios.gdxengine.entity;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.kotcrab.vis.ui.util.adapter.ArrayListAdapter;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.silencestudios.gdxengine.utils.Consumer;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class ComponentsListAdapter extends ArrayListAdapter<Component, VisTable> {

    public ComponentsListAdapter(ArrayList<Component> array) {
        super(array);
    }

    @Override
    protected VisTable createView(Component item) {
        VisTable visTable = new VisTable();
        visTable.left().top();

        Class<?> clazz = item.getClass();
        visTable.add(new VisLabel(clazz.getSimpleName()))
                .expandX()
                .fillX()
                .row();

        for (Field field : clazz.getDeclaredFields()) {
            try {
                field.setAccessible(true);
                String key = field.getName();

                visTable.pad(8f);
                visTable.add(key).expandX().fillX();

                Widget actor = getWidget(field, item);

                Cell value = visTable.add().expandX().fillX();
                value.setActor(actor);

                visTable.row();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return visTable;
    }

    @Override
    protected void updateView(VisTable view, Component item) {
        view.clear();
        view.left().top();

        Class<?> clazz = item.getClass();
        view.add(new VisLabel(clazz.getSimpleName()))
                .expandX()
                .fillX()
                .row();

        for (Field field : clazz.getDeclaredFields()) {
            try {
                field.setAccessible(true);
                String key = field.getName();

                view.pad(8f);
                view.add(key).expandX().fillX();

                Widget actor = getWidget(field, item);

                Cell value = view.add().expandX().fillX();
                value.setActor(actor);

                view.row();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private Widget getWidget(Field field, Component item) throws IllegalAccessException {
        Widget actor;
        if (field.getType().isPrimitive())
            actor = new VisLabel(field.get(item).toString());
        else if (field.getType() == Vector2.class) {
            actor = new VisLabel(field.get(item).toString());
        } else if (field.getType() == Runnable.class) {
            actor = new VisLabel("Runnable");
        } else if (field.getType() == Consumer.class) {
            actor = new VisLabel("Consumer");
        } else if (field.get(item) != null) {
            actor = new VisLabel(field.get(item).getClass().getSimpleName());
        } else {
            actor = new VisLabel(field.getClass().getSimpleName());
        }

        return actor;
    }
}
