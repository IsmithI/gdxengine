package com.silencestudios.gdxengine.component;

import com.badlogic.ashley.core.Entity;
import com.silencestudios.gdxengine.instance.ComponentInjector;

import org.junit.Test;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class TransformTest {

    private ComponentInjector componentInjector = new ComponentInjector();

    @Test
    public void canAddParentWithTransformComponent() {
        TestEntity entityParent = new TestEntity();
        componentInjector.process(entityParent);

        TestEntity entityChild = new TestEntity();
        componentInjector.process(entityChild);

        Transform childTransform = entityChild.getComponent(Transform.class);
        Transform parentTransform = entityParent.getComponent(Transform.class);

        childTransform.setParent(entityParent);

        assertSame(childTransform.getParent(), entityParent);

        boolean found = false;
        for (Entity entity : parentTransform.getChildren()) {
            if (entity.getComponent(Transform.class).getParent() == entityParent)
                found = true;
        }

        assertTrue(found);
    }

    @Test
    public void canAddChildWithTransform() {
        TestEntity entityParent = new TestEntity();
        componentInjector.process(entityParent);

        TestEntity entityChild = new TestEntity();
        componentInjector.process(entityChild);

        Transform childTransform = entityChild.getComponent(Transform.class);
        Transform parentTransform = entityParent.getComponent(Transform.class);

        parentTransform.addChild(entityChild);

        assertSame(childTransform.getParent(), entityParent);

        boolean found = false;
        for (Entity entity : parentTransform.getChildren()) {
            if (entity.getComponent(Transform.class).getParent() == entityParent)
                found = true;
        }

        assertTrue(found);
    }
}
