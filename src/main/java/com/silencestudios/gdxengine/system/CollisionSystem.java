package com.silencestudios.gdxengine.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;

import java.util.LinkedList;
import java.util.List;

public class CollisionSystem extends EntitySystem {

    private World world;
    private Engine engine;
    private List<AbstractCollisionSystem> abstractCollisionSystems = new LinkedList<>();

    public CollisionSystem(World world, Engine engine) {
        this.world = world;
        this.engine = engine;
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        Gdx.app.log("CollisionSystem", "registered listener");
        world.setContactListener(new ContactListener() {
            @Override
            public void beginContact(Contact contact) {
                Object dataA = contact.getFixtureA().getUserData();
                Object dataB = contact.getFixtureB().getUserData();

                if (dataA instanceof Entity && dataB instanceof Entity) {
                    Entity entityA = (Entity) dataA;
                    Entity entityB = (Entity) dataB;

                    for (AbstractCollisionSystem abstractCollisionSystem : abstractCollisionSystems) {
                        if (abstractCollisionSystem.canApplyA(entityA) && abstractCollisionSystem.canApplyB(entityB)) {
                            abstractCollisionSystem.handleContactBegin(entityA, entityB);
                        } else if (abstractCollisionSystem.canApplyA(entityB) && abstractCollisionSystem.canApplyB(entityA)) {
                            abstractCollisionSystem.handleContactBegin(entityB, entityA);
                        }
                    }
                }
            }

            @Override
            public void endContact(Contact contact) {
                Object dataA = contact.getFixtureA().getUserData();
                Object dataB = contact.getFixtureB().getUserData();

                if (dataA instanceof Entity && dataB instanceof Entity) {
                    Entity entityA = (Entity) dataA;
                    Entity entityB = (Entity) dataB;

                    for (AbstractCollisionSystem abstractCollisionSystem : abstractCollisionSystems) {
                        if (abstractCollisionSystem.canApplyA(entityA) && abstractCollisionSystem.canApplyB(entityB)) {
                            abstractCollisionSystem.handleContactEnd(entityA, entityB);
                        } else if (abstractCollisionSystem.canApplyA(entityB) && abstractCollisionSystem.canApplyB(entityA)) {
                            abstractCollisionSystem.handleContactEnd(entityB, entityA);
                        }
                    }
                }
            }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {

            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {

            }
        });
    }

    @Override
    public void removedFromEngine(Engine engine) {
        super.removedFromEngine(engine);

        world.setContactListener(null);
    }

    public void addListener(AbstractCollisionSystem abstractCollisionSystem) {
        abstractCollisionSystems.add(abstractCollisionSystem);
        engine.addSystem(abstractCollisionSystem);
    }

    public void removeListener(AbstractCollisionSystem abstractCollisionSystem) {
        abstractCollisionSystems.remove(abstractCollisionSystem);
        engine.removeSystem(abstractCollisionSystem);
    }
}
