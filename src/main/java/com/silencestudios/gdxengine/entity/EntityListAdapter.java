package com.silencestudios.gdxengine.entity;

import com.badlogic.ashley.core.Entity;
import com.kotcrab.vis.ui.util.adapter.ArrayListAdapter;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.silencestudios.gdxengine.component.ID;
import com.silencestudios.gdxengine.component.annotations.Dependency;

import java.util.ArrayList;

public class EntityListAdapter extends ArrayListAdapter<Entity, VisTable> {

    public EntityListAdapter(ArrayList<Entity> array) {
        super(array);
        setSelectionMode(SelectionMode.SINGLE);
    }

    @Override
    protected VisTable createView(Entity item) {
        VisTable table = new VisTable();
        table.top().left();

        Class<?> itemClass = item.getClass();
        String label = itemClass.getSimpleName();
        if (itemClass.isAnnotationPresent(Dependency.class)) {
            label += " (" + itemClass.getAnnotation(Dependency.class).ID() + ")";
        }
        ID id;
        if ((id = item.getComponent(ID.class)) != null) {
            label = itemClass.getSimpleName() + " (" + id.value + ") ";
        }

        table.add(new VisLabel(label));

        return table;
    }

    @Override
    protected void selectView(VisTable view) {
        view.setBackground("selection");
    }

    @Override
    protected void deselectView(VisTable view) {
        view.setBackground("grey");
    }
}
