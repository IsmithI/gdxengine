package com.silencestudios.gdxengine.input;

import com.badlogic.gdx.Gdx;

import java.util.HashMap;

public class Input {

    private static Input instance = null;
    private HashMap<ActionKeys, int[]> map;

    public Input() {
        map = new HashMap<>();

        map.put(ActionKeys.LEFT, new int[]{
                com.badlogic.gdx.Input.Keys.A,
                com.badlogic.gdx.Input.Keys.DPAD_LEFT
        });

        map.put(ActionKeys.RIGHT, new int[]{
                com.badlogic.gdx.Input.Keys.D,
                com.badlogic.gdx.Input.Keys.DPAD_RIGHT
        });

        map.put(ActionKeys.UP, new int[]{
                com.badlogic.gdx.Input.Keys.W,
                com.badlogic.gdx.Input.Keys.DPAD_UP
        });

        map.put(ActionKeys.DOWN, new int[]{
                com.badlogic.gdx.Input.Keys.S,
                com.badlogic.gdx.Input.Keys.DPAD_DOWN
        });

        map.put(ActionKeys.JUMP, new int[]{
                com.badlogic.gdx.Input.Keys.SPACE,
                com.badlogic.gdx.Input.Keys.BUTTON_X
        });
    }

    public static Input get() {
        if (instance == null)
            instance = new Input();

        return instance;
    }

    public boolean isActionPressed(ActionKeys actionKey) {
        if (map.containsKey(actionKey)) {
            int[] keys = map.get(actionKey);
            for (int key : keys) {
                if (Gdx.input.isKeyPressed(key)) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean isActionJustPressed(ActionKeys actionKey) {
        if (map.containsKey(actionKey)) {
            int[] keys = map.get(actionKey);
            for (int key : keys) {
                if (Gdx.input.isKeyJustPressed(key)) {
                    return true;
                }
            }
        }

        return false;
    }

    public HashMap<ActionKeys, int[]> getMap() {
        return map;
    }
}
