package com.silencestudios.gdxengine.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.World;
import com.silencestudios.gdxengine.component.PhysicsBody;
import com.silencestudios.gdxengine.component.Transform;
import com.silencestudios.gdxengine.config.PhysicsConfig;

public class PhysicsSystem extends IteratingSystem {

    private final ComponentMapper<PhysicsBody> pm = ComponentMapper.getFor(PhysicsBody.class);
    private final ComponentMapper<Transform> tm = ComponentMapper.getFor(Transform.class);

    private World world;
    private PhysicsConfig physicsConfig = PhysicsConfig.get();
    private float accumulator = 0;

    public PhysicsSystem(World world) {
        super(Family.all(PhysicsBody.class, Transform.class).get());
        this.world = world;
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        engine.addEntityListener(Family.all(PhysicsBody.class, Transform.class).get(), new EntityListener() {
            @Override
            public void entityAdded(Entity entity) {
                Transform transform = tm.get(entity);
                PhysicsBody physicsBody = pm.get(entity);

                physicsBody.bodyDef.position.set(transform.getPosition());

                physicsBody.body = world.createBody(physicsBody.bodyDef);
                physicsBody.fixture = physicsBody.body.createFixture(physicsBody.fixtureDef);

                physicsBody.body.setUserData(entity);
                physicsBody.fixture.setUserData(entity);

                transform.onPositionChange = position -> {
                    physicsBody.body.setTransform(position, transform.getRotation() * MathUtils.degRad);
                };
            }

            @Override
            public void entityRemoved(Entity entity) {
                PhysicsBody physicsBody = pm.get(entity);

                world.destroyBody(physicsBody.body);
                physicsBody.body.setUserData(null);
                physicsBody.fixture.setUserData(null);
                physicsBody.body = null;
            }
        });
    }

    @Override
    public void update(float deltaTime) {
        float frameTime = Math.min(deltaTime * 2f, 0.25f);
        accumulator += frameTime;

        while (accumulator >= PhysicsConfig.get().getStepDt()) {
            world.step(physicsConfig.getStepDt(), physicsConfig.getVelocityIterations(), physicsConfig.getPositionIterations());
            accumulator -= PhysicsConfig.get().getStepDt();
        }

        super.update(deltaTime);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        Transform transform = tm.get(entity);
        PhysicsBody physicsBody = pm.get(entity);

        transform.forceMove(physicsBody.body.getPosition());
    }
}
