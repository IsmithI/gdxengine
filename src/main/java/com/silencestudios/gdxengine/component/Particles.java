package com.silencestudios.gdxengine.component;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;

public class Particles implements Drawer {

    public boolean paused = false;
    private ParticleEffect particleEffect;

    public Particles(ParticleEffect particleEffect) {
        this.particleEffect = particleEffect;
    }

    @Override
    public void draw(Batch batch, float dt, float x, float y, float scaleX, float scaleY, float rotation) {
        particleEffect.setPosition(x, y);
        if (!paused)
            particleEffect.draw(batch, dt);
    }

    public void start() {
        particleEffect.start();
    }

    public ParticleEffect getEffect() {
        return particleEffect;
    }
}
