package com.silencestudios.gdxengine.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.silencestudios.gdxengine.utils.Consumer;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Transform implements Component {

    public Consumer<Vector2> onPositionChange = position -> {
    };

    private Entity entity;
    private Entity parent;
    private List<Entity> children = new LinkedList<>();

    private int z = 0;
    private Vector2 position = new Vector2(0, 0);
    private Vector2 scale = new Vector2(1, 1);
    private float rotation = 0f;

    public Transform() {
    }

    public Transform(Entity entity) {
        this.entity = entity;
    }

    public Vector2 getPosition() {
        if (parent != null) {
            Transform t = parent.getComponent(Transform.class);
            return new Vector2(t.getPosition()).add(new Vector2(position).rotate(t.getRotation()));
        }

        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;

        onPositionChange.accept(getPosition());
    }

    public Vector2 getScale() {
        return scale;
    }

    public void setScale(Vector2 scale) {
        this.scale = scale;
    }

    public float getRotation() {
        if (parent != null) {
            Transform t = parent.getComponent(Transform.class);
            return t.getRotation() + rotation;
        }

        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;

        onPositionChange.accept(getPosition());
    }

    public void moveTo(Vector2 position) {
        this.position.x = position.x;
        this.position.y = position.y;

        onPositionChange.accept(getPosition());
    }

    public void moveTo(float x, float y) {
        this.position.x = x;
        this.position.y = y;

        onPositionChange.accept(getPosition());
    }

    public void moveBy(Vector2 position) {
        this.position.x += position.x;
        this.position.y += position.y;

        onPositionChange.accept(this.position);
    }

    public void moveBy(float x, float y) {
        this.position.x += x;
        this.position.y += y;

        onPositionChange.accept(getPosition());
    }

    public void forceMove(float x, float y) {
        this.position.x = x;
        this.position.y = y;
    }

    public void forceMove(Vector2 pos) {
        this.position.x = pos.x;
        this.position.y = pos.y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public Entity getParent() {
        return parent;
    }

    public void setParent(Entity parent) {
        parent.getComponent(Transform.class).addChild(entity);
        onPositionChange.accept(getPosition());
    }

    public Entity getEntity() {
        return entity;
    }

    public void addChild(Entity child) {
        children.add(child);
        child.getComponent(Transform.class).parent = entity;
    }

    public List<Entity> getChildren() {
        return Collections.unmodifiableList(children);
    }
}
