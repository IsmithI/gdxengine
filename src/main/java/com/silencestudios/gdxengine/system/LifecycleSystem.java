package com.silencestudios.gdxengine.system;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.silencestudios.gdxengine.component.Lifecycle;
import com.silencestudios.gdxengine.component.Updater;

public class LifecycleSystem extends IteratingSystem {

    public LifecycleSystem() {
        super(Family.one(Lifecycle.class).get());
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        engine.addEntityListener(Family.one(Lifecycle.class).get(), new EntityListener() {
            @Override
            public void entityAdded(Entity entity) {
                entity.getComponent(Lifecycle.class).onCreate.run();
            }

            @Override
            public void entityRemoved(Entity entity) {
                entity.getComponent(Lifecycle.class).onRemove.run();
            }
        });
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        entity.getComponent(Lifecycle.class).onUpdate.accept(deltaTime);

        for (Component component : entity.getComponents()) {
            if (component instanceof Updater) {
                ((Updater) component).update(deltaTime);
            }
        }
    }
}
