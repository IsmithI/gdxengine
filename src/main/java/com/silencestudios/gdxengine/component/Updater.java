package com.silencestudios.gdxengine.component;

import com.badlogic.ashley.core.Component;

public interface Updater extends Component {
    void update(float dt);
}
