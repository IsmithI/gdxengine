package com.silencestudios.gdxengine.screen;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.silencestudios.gdxengine.component.AssetsContainer;
import com.silencestudios.gdxengine.component.BatchContainer;
import com.silencestudios.gdxengine.component.Camera;
import com.silencestudios.gdxengine.component.ID;
import com.silencestudios.gdxengine.component.Transform;
import com.silencestudios.gdxengine.config.DebugConfig;
import com.silencestudios.gdxengine.config.PhysicsConfig;
import com.silencestudios.gdxengine.config.ScreenConfig;
import com.silencestudios.gdxengine.entity.Assets;
import com.silencestudios.gdxengine.entity.ComponentsList;
import com.silencestudios.gdxengine.entity.EntitiesList;
import com.silencestudios.gdxengine.entity.HUD;
import com.silencestudios.gdxengine.entity.MainCamera;
import com.silencestudios.gdxengine.entity.PhysicsWorld;
import com.silencestudios.gdxengine.instance.Instance;
import com.silencestudios.gdxengine.system.ActorSystem;
import com.silencestudios.gdxengine.system.CollisionSystem;
import com.silencestudios.gdxengine.system.InputProcessingSystem;
import com.silencestudios.gdxengine.system.LifecycleSystem;
import com.silencestudios.gdxengine.system.PhysicsSystem;
import com.silencestudios.gdxengine.system.Rendering;

public abstract class BaseScreen implements Screen {

    private final ScreenConfig screenConfig;
    protected final AssetManager assetManager;
    protected InputMultiplexer inputMultiplexer;
    protected Engine engine;
    protected World world;
    protected Box2DDebugRenderer debugRenderer;
    protected CollisionSystem collisionSystem;
    protected EventDispatcher eventDispatcher;
    protected Stage stage;
    protected OrthographicCamera camera;
    private Batch batch;

    public BaseScreen(AssetManager assetManager, ScreenConfig screenConfig) {
        this.assetManager = assetManager;
        this.screenConfig = screenConfig;
        engine = new Engine();
        batch = new SpriteBatch();
        world = new World(PhysicsConfig.get().getGravity(), true);

        camera = new OrthographicCamera();

        Viewport viewport = new ExtendViewport(screenConfig.worldWidth, screenConfig.worldHeight, camera);
        Viewport stageViewport = new ExtendViewport(1000 / screenConfig.guiScale, 1000 / getRatio() / screenConfig.guiScale, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        stage = new Stage(stageViewport);

        debugRenderer = new Box2DDebugRenderer();

        Entity batchEntity = new Entity();
        batchEntity.add(new ID("batch"));
        batchEntity.add(new BatchContainer(batch));
        engine.addEntity(batchEntity);

        MainCamera cameraEntity = new MainCamera();
        Camera cameraComponent = new Camera();
        cameraComponent.setCamera(camera);
        cameraComponent.setViewport(viewport);

        Transform cameraTransform = new Transform();
        if (screenConfig.centerCamera) {
            cameraTransform.moveTo(screenConfig.worldWidth / 2f, screenConfig.worldHeight / 2f);
        }
        cameraEntity.add(cameraTransform);
        cameraEntity.add(cameraComponent);

        engine.addEntity(cameraEntity);

        Assets assets = new Assets();
        assets.assetsContainer = new AssetsContainer(assetManager);
        engine.addEntity(assets);

        Instance.init(engine, assetManager);
        inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(stage);

        eventDispatcher = (EventDispatcher) Instance.get().create(EventDispatcher.class);
        eventDispatcher.setEngine(engine);

        PhysicsWorld physicsWorld = (PhysicsWorld) Instance.get().create(PhysicsWorld.class);
        physicsWorld.setWorld(world);

        engine.addSystem(new Rendering(batch));
        engine.addSystem(new ActorSystem(stage));
        engine.addSystem(new InputProcessingSystem(inputMultiplexer));
        engine.addSystem(new PhysicsSystem(world));

        collisionSystem = new CollisionSystem(world, engine);
        engine.addSystem(collisionSystem);

        engine.addSystem(new LifecycleSystem());

        configureEngine();

        createDebugHUD();
    }

    public static float getRatio() {
        return ((float) Gdx.graphics.getWidth()) / ((float) Gdx.graphics.getHeight());
    }

    private void createDebugHUD() {
        if (DebugConfig.get().isEnabled()) {
            Instance.get().create(EntitiesList.class);
            Instance.get().create(ComponentsList.class);
            Instance.get().create(HUD.class);
        }
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    protected abstract void configureEngine();

    @Override
    public void render(float delta) {
        Color color = screenConfig.clearScreenColor;

        Gdx.gl.glClearColor(color.r, color.g, color.b, color.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        engine.update(delta);
        batch.end();

        stage.act(delta);
        stage.getCamera().update();
        stage.getViewport().apply();
        stage.draw();

        if (PhysicsConfig.get().isDrawDebug())
            debugRenderer.render(world, camera.combined);
    }

    @Override
    public void resize(int width, int height) {
        ImmutableArray<Entity> entities = engine.getEntitiesFor(Family.one(Camera.class).get());
        for (Entity entity : entities) {
            Viewport viewport = entity.getComponent(Camera.class).getViewport();
            Gdx.app.log("Screen", "resize: " + width + ":" + height);
            viewport.update(width, height);
        }

        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
