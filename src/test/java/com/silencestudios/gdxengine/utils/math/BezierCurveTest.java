package com.silencestudios.gdxengine.utils.math;

import com.badlogic.gdx.math.Vector2;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BezierCurveTest {

    @Test
    public void testPowMethod() {
        BezierCurve bezierCurve = new BezierCurve();
        assertEquals(8, bezierCurve.pow(2, 3), 0.0);
        assertEquals(2, bezierCurve.pow(2, 1), 0.0);
        assertEquals(1, bezierCurve.pow(2, 0), 0.0);
    }

    @Test
    public void testBasisCalculation() {
        BezierCurve bezierCurve = new BezierCurve();

        bezierCurve.getPoints().add(new Vector2(0, 0));
        bezierCurve.getPoints().add(new Vector2(1, 1));
        bezierCurve.getPoints().add(new Vector2(1.2f, 0.4f));
        bezierCurve.getPoints().add(new Vector2(2, 0));

        List<Vector2> result = new LinkedList<>();
        for (float t = 0; t < 1; t += 0.1) {
            result.add(bezierCurve.calculate(t));
        }

        assertTrue(true);
    }
}
