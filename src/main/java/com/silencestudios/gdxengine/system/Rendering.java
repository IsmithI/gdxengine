package com.silencestudios.gdxengine.system;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.SortedIteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.silencestudios.gdxengine.component.Camera;
import com.silencestudios.gdxengine.component.Drawer;
import com.silencestudios.gdxengine.component.Transform;

public class Rendering extends SortedIteratingSystem {

    private ComponentMapper<Camera> cm = ComponentMapper.getFor(Camera.class);
    private ComponentMapper<Transform> tm = ComponentMapper.getFor(Transform.class);

    private Batch batch;
    private ImmutableArray<Entity> cameraEntities;

    public Rendering(Batch batch) {
        this(Family.all(Transform.class).get(), batch);
    }

    public Rendering(Family family, Batch batch) {
        super(family, (a, b) -> {
            Transform transformA = a.getComponent(Transform.class);
            Transform transformB = b.getComponent(Transform.class);

            return transformA.getZ() - transformB.getZ();
        });
        this.batch = batch;
    }


    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        forceSort();
        Transform transform = tm.get(entity);
        cameraEntities = getEngine().getEntitiesFor(Family.all(Camera.class, Transform.class).get());

        for (Entity camera : cameraEntities) {
            Camera c = cm.get(camera);
            Transform cameraTransform = tm.get(camera);

            c.getCamera().position.set(cameraTransform.getPosition().x, cameraTransform.getPosition().y, c.getCamera().position.z);
            c.getCamera().update();
            c.getViewport().apply();
            batch.setProjectionMatrix(c.getCamera().combined);

            for (Component component : entity.getComponents()) {
                if (component instanceof Drawer) {
                    Drawer drawer = (Drawer) component;
                    Vector2 position = transform.getPosition();
                    Vector2 scale = transform.getScale();

                    drawer.draw(batch, deltaTime, position.x, position.y, scale.x, scale.y, transform.getRotation());
                }
            }
        }
    }
}
