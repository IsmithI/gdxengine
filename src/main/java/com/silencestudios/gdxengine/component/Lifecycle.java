package com.silencestudios.gdxengine.component;

import com.badlogic.ashley.core.Component;
import com.silencestudios.gdxengine.utils.Consumer;

public class Lifecycle implements Component {
    public Runnable onCreate = () -> {
    };

    public Consumer<Float> onUpdate = delta -> {
    };

    public Runnable onRemove = () -> {
    };
}
