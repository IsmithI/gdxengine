package com.silencestudios.gdxengine.component;

import com.silencestudios.gdxengine.utils.Event;

public interface EventListener<T> {
    void onEvent(Event<T> event);

    String getEventType();
}