package com.silencestudios.gdxengine.utils;

public class Event<T> {
    public String name;
    public T data;
}
