package com.silencestudios.gdxengine.instance;

import com.silencestudios.gdxengine.component.Transform;

import org.junit.Test;

import static org.junit.Assert.assertSame;

public class ComponentInjectorTest {

    private ComponentInjector componentInjector = new ComponentInjector();

    @Test
    public void entityInstanceInjectsIntoTransformField() {
        TestEntity entity = new TestEntity();
        componentInjector.process(entity);

        Transform transform = entity.getComponent(Transform.class);

        assertSame(transform.getEntity(), entity);
    }

    @Test
    public void entityInstanceInjectsToTransformConstructedWithMethod() {
        TestEntityMethod testEntityMethod = new TestEntityMethod();
        componentInjector.process(testEntityMethod);

        Transform transform = testEntityMethod.getComponent(Transform.class);

        assertSame(transform.getEntity(), testEntityMethod);
    }
}
