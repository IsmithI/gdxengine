package com.silencestudios.gdxengine.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.InputProcessor;

public class InputProcessorContainer implements Component {

    private InputProcessor inputProcessor;

    public InputProcessorContainer(InputProcessor inputProcessor) {
        this.inputProcessor = inputProcessor;
    }

    public InputProcessor getInputProcessor() {
        return inputProcessor;
    }

    public void setInputProcessor(InputProcessor inputProcessor) {
        this.inputProcessor = inputProcessor;
    }
}
