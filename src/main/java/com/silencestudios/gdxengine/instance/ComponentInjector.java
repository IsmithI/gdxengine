package com.silencestudios.gdxengine.instance;


import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.silencestudios.gdxengine.component.Provider;
import com.silencestudios.gdxengine.component.Transform;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ComponentInjector {

    public void process(Entity entity) {
        Class<?> clazz = entity.getClass();

        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(ComponentProvider.class)) {
                ComponentProvider componentProvider = field.getAnnotation(ComponentProvider.class);
                field.setAccessible(true);
                Class<?> componentClass = field.getType();
                try {
                    Provider provider;
                    Class<? extends Provider> providerClass = componentProvider.of();
                    if (providerClass != ComponentProvider.Default.class) {
                        provider = providerClass.getConstructor(Entity.class).newInstance(entity);

                        Component component = provider.get();
                        injectEntity(component, entity);
                        field.set(entity, component);
                        entity.add(component);
                    } else {
                        Component component = (Component) componentClass.newInstance();
                        injectEntity(component, entity);
                        field.set(entity, component);
                        entity.add(component);
                    }
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }

        for (Method method : clazz.getDeclaredMethods()) {
            if (method.isAnnotationPresent(ComponentProvider.class)) {

                method.setAccessible(true);
                try {
                    Component component = (Component) method.invoke(entity);
                    injectEntity(component, entity);
                    entity.add(component);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void injectEntity(Component component, Entity entity) {
        try {
            if (component instanceof Transform) {
                Field entityField = component.getClass().getDeclaredField("entity");
                if (entityField != null) {
                    entityField.setAccessible(true);
                    entityField.set(component, entity);
                }
            }
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
}
