package com.silencestudios.gdxengine.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.silencestudios.gdxengine.config.ScreenConfig;
import com.silencestudios.gdxengine.utils.math.BezierCurve;

public final class ScreenUtils {

    public static float getWorldX(float screenX) {
        return (screenX / (float) Gdx.graphics.getWidth()) * ScreenConfig.get().worldWidth;
    }

    public static float getWorldY(float screenY) {
        float yy = Gdx.graphics.getHeight() - screenY;
        return (yy / (float) Gdx.graphics.getHeight()) * ScreenConfig.get().worldHeight;
    }

    public static Vector2 getWorldCoords(Vector2 screen) {
        return new Vector2(getWorldX(screen.x), getWorldY(screen.y));
    }
}
