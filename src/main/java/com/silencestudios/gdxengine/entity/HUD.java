package com.silencestudios.gdxengine.entity;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisSplitPane;
import com.kotcrab.vis.ui.widget.VisTable;
import com.silencestudios.gdxengine.component.ActorContainer;
import com.silencestudios.gdxengine.component.Transform;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;
import com.silencestudios.gdxengine.component.annotations.Debug;
import com.silencestudios.gdxengine.component.annotations.Dependency;
import com.silencestudios.gdxengine.screen.BaseScreen;

@Debug
public class HUD extends Entity {

    @ComponentProvider
    private Transform transform;

    @Dependency(ID = "entitiesList")
    private EntitiesList entitiesList;

    @Dependency(ID = "componentsList")
    private ComponentsList componentsList;

    private Entity selectedEntity;

    @ComponentProvider
    public ActorContainer actorContainer() {

        VisTable table = new VisTable();
        table.setBackground("default-pane");
        table.setTouchable(Touchable.childrenOnly);
        table.top().left();
        table.setSize(400f, Gdx.graphics.getHeight() / 2f);

        Cell titleCell = table.add(new VisLabel("Debug"));
        titleCell
                .pad(8f)
                .expandX()
                .fillX()
                .row();

        Cell contentCell = table.add().expand();
        contentCell.pad(8f);

        entitiesList.getList().setItemClickListener(this::handleEntitySelect);

        VisTable entitiesTable = new VisTable();
        entitiesTable.top().left();
        entitiesTable.add(new VisLabel("Entities")).expandX().fillX();
        entitiesTable.row();
        entitiesTable.addSeparator();
        entitiesTable.row();
        entitiesTable.add(entitiesList.getActor()).grow();

        VisTable componentsTable = new VisTable();
        entitiesTable.top().left();
        componentsTable.add(new VisLabel("Components")).expandX().fillX();
        componentsTable.row();
        componentsTable.addSeparator().row();
        componentsTable.add(componentsList.getActor()).grow();

        VisSplitPane splitPane = new VisSplitPane(entitiesTable, componentsTable, true);

        Vector2 start = new Vector2();
        titleCell.getActor().addListener(new DragListener() {
            @Override
            public void dragStart(InputEvent event, float x, float y, int pointer) {
                start.set(x, y);
            }

            @Override
            public void drag(InputEvent event, float x, float y, int pointer) {
                transform.moveTo(Gdx.input.getX() / BaseScreen.getRatio() - start.x, Gdx.graphics.getHeight() / BaseScreen.getRatio() - Gdx.input.getY() + start.y);
            }
        });

        transform.moveTo(Gdx.graphics.getWidth() - 400f, 0);

        contentCell.setActor(splitPane).fill().top().left();

        return new ActorContainer(table);
    }

    private void handleEntitySelect(Entity entity) {
        selectedEntity = entity;
        Gdx.app.log("HUD", "Clicked " + entity.toString());
        componentsList.setSelectedEntity(entity);
    }
}
