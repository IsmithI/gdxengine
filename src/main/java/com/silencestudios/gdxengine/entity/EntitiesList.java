package com.silencestudios.gdxengine.entity;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kotcrab.vis.ui.widget.ListView;
import com.silencestudios.gdxengine.component.Lifecycle;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;
import com.silencestudios.gdxengine.component.annotations.Debug;
import com.silencestudios.gdxengine.component.annotations.Dependency;

import java.util.ArrayList;

@Debug
@Dependency(ID = "entitiesList")
public class EntitiesList extends Entity {

    public Engine engine;

    private ListView<Entity> entityListView;
    private EntityListAdapter entityListAdapter;

    @ComponentProvider
    public Lifecycle lifecycle() {
        Lifecycle lifecycle = new Lifecycle();

        ArrayList<Entity> entities = new ArrayList<>();
        for (Entity entity : engine.getEntities()) {
            if (!entity.getClass().isAnnotationPresent(Debug.class))
                entities.add(entity);
        }

        entityListAdapter = new EntityListAdapter(entities);
        entityListView = new ListView<>(entityListAdapter);

        engine.addEntityListener(new EntityListener() {
            @Override
            public void entityAdded(Entity entity) {
                if (!entity.getClass().isAnnotationPresent(Debug.class)) {
                    entities.add(entity);
                    entityListAdapter.itemsChanged();
                }
            }

            @Override
            public void entityRemoved(Entity entity) {
                entities.remove(entity);
                entityListAdapter.itemsChanged();
            }
        });

        return lifecycle;
    }

    public Actor getActor() {
        return entityListView.getMainTable();
    }

    public ListView<Entity> getList() {
        return entityListView;
    }
}
