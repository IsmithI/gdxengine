package com.silencestudios.gdxengine.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.silencestudios.gdxengine.component.ActorContainer;
import com.silencestudios.gdxengine.component.Transform;

public class ActorSystem extends IteratingSystem {

    private Stage stage;
    private EntityListener listener;

    public ActorSystem(Stage stage) {
        super(Family.all(Transform.class, ActorContainer.class).get());
        this.stage = stage;
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        listener = new EntityListener() {
            @Override
            public void entityAdded(Entity entity) {
                Actor actor = entity.getComponent(ActorContainer.class).getActor();
                stage.addActor(actor);
            }

            @Override
            public void entityRemoved(Entity entity) {
                Actor actor = entity.getComponent(ActorContainer.class).getActor();
                actor.remove();
            }
        };

        engine.addEntityListener(getFamily(), listener);
    }

    @Override
    public void removedFromEngine(Engine engine) {
        super.removedFromEngine(engine);

        engine.removeEntityListener(listener);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        Transform transform = entity.getComponent(Transform.class);
        Actor actor = entity.getComponent(ActorContainer.class).getActor();

        actor.setPosition(transform.getPosition().x, transform.getPosition().y);
        actor.setRotation(transform.getRotation());
        actor.setScale(transform.getScale().x, transform.getScale().y);
    }
}
